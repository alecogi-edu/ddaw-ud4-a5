<?php


class MySQLSessionHandler implements SessionHandlerInterface
{

    /**
     * @var Mysqli Connection
     */
    private $connection;

    private $host;

    private $user;

    private $password;

    private $database;

    public function __construct(string $host, string $user, string $password, string $database = 'php_sessions')
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->database = $database;
    }

    /**
     * Open new Mysql Connection
     *
     * @param string $savePath
     * @param string $sessionName
     * @return bool
     */
    public function open($savePath, $sessionName)
    {

        $connection = mysqli_connect($this->host,$this->user,$this->password,$this->database);

        if($connection){
            $this->connection = $connection;
            return true;
        }
        return false;

    }

    /**
     * Close the MysqlConnection
     *
     * @return bool
     */
    public function close(): bool
    {

        mysqli_close($this->connection);
        return true;

    }

    /**
     * Read session data stored for id
     *
     * @param string $id
     * @return string
     */
    public function read($id): string
    {

        $sql = "SELECT data FROM sessions WHERE id = '$id' AND expires > '".date('Y-m-d H:i:s')."'";

        $result = mysqli_query($this->connection, $sql);

        if($row = mysqli_fetch_assoc($result)){
            return $row['data'];
        }else{
            return "";
        }

    }

    /**
     * Update Data for stored Session id
     *
     * @param string $id
     * @param string $data
     * @return bool
     * @throws Exception
     **/
    public function write($id, $data): bool
    {

        $nowDateTime = new DateTime();
        $nowDateTime->modify("+ 1 hour");
        $nowDateTimeString = $nowDateTime->format('Y-m-d H:i:s');

        $sql = "INSERT INTO sessions (id,expires,data) VALUES ('$id', '$nowDateTimeString', '$data')
                ON DUPLICATE KEY UPDATE id='$id', expires='$nowDateTimeString', data='$data'";

        $result = mysqli_query($this->connection, $sql);

        return ($result) ? true : false;

    }

    /**
     * Destroy a session with @id
     *
     * @param string $id
     * @param string $data
     * @return bool
     * @throws Exception
     */
    public function destroy($id): bool
    {

        $sql = "DELETE FROM sessions WHERE id ='$id'";
        $result = mysqli_query($this->connection, $sql);

        return ($result) ? true : false;

    }

    /**
     * Cleans up expired sessions.
     *
     * @param string $id
     * @param string $data
     * @return bool
     * @throws Exception
     */
    public function gc($maxlifetime): bool
    {

        $sql = "DELETE FROM sessions WHERE ((UNIX_TIMESTAMP(expires) + ".$maxlifetime.") < ".$maxlifetime.")";
        $result = mysqli_query($this->connection, $sql);

        return ($result) ? true : false;

    }

}

